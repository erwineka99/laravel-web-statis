<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
            <option value="Other">Other</option>
        </select><br><br>
        Languange Spoken:<br><br>
        <input type="checkbox" value="Bahasa_Indonesia"> Bahasa Indonesia<br>
        <input type="checkbox" value="English"> English<br>
        <input type="checkbox" value="Other"> Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
