<?php

namespace App\Http\Controllers;

    use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){

        return view('register');
    }

    public function welcome(Request $request){
        //dd($request->all());
        $f_name=$request['first_name'];
        $l_name=$request['last_name'];
        $name=$f_name." ".$l_name;
        return view('welcome')->with('name',$name);
        }
}

?>
